import React from 'react';
import {CarouselProvider, Dot, Image, Slide, Slider} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import './CustomCarousel.scss';

export class CustomCarousel extends React.Component<{}, {}> {
    render(): JSX.Element {
        return <CarouselProvider
            className='CustomCarousel'
            naturalSlideWidth={100}
            naturalSlideHeight={30}
            totalSlides={4}
            interval={4000}
            isPlaying={true}
        >
            <Slider>
                <Slide index={0}>
                    <Image
                        className='sliderImg'
                        src='images/fullhd/woman1.jpg'
                        hasMasterSpinner={true}
                    />
                </Slide>
                <Slide index={1}>
                    <Image
                        className='sliderImg'
                        src='images/fullhd/woman2.jpg'
                        hasMasterSpinner={true}
                    />
                </Slide>
                <Slide index={2}>
                    <Image
                        className='sliderImg'
                        src='images/fullhd/woman3.jpg'
                        hasMasterSpinner={true}
                    />
                </Slide>
                <Slide index={3}>
                    <Image
                        className='sliderImg'
                        src='images/fullhd/woman4.jpg'
                        hasMasterSpinner={true}
                    />
                </Slide>
            </Slider>
            <Dot
                children=''
                slide={0}
                className='sliderDotsFirst sliderDot'
            />
            <Dot
                children=''
                slide={1}
                className='sliderDots1 sliderDot'
            />
            <Dot
                children=''
                slide={2}
                className='sliderDots2 sliderDot'
            />
            <Dot
                children=''
                slide={3}
                className='sliderDots3 sliderDot'
            />
        </CarouselProvider>;
    }
}
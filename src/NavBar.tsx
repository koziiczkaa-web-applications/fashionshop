import React from 'react';
import './NavBar.scss';

export interface NavBarProps {
    className?: string;
}

export class NavBar extends React.Component<NavBarProps, {}> {
    render(): JSX.Element {
        return <nav className={`${this.props.className} Nav`}>
            <ul className='navigation'>
                <li className='nav-element logo'>
                    <a className="link" href="#">
                        <img className='logo-img' src="images/logo.png"/>
                    </a>
                </li>
                <li className='nav-element'>
                    <a className="link" href="#">ODZIEŻ</a>
                </li>
                <li className='nav-element'>
                    <a className="link" href="#">OBUWIE</a>
                </li>
                <li className='nav-element'>
                    <a className="link" href="#">KONTAKT</a>
                </li>
                <li className='nav-element panel rej'>
                    <a className='link' href="#">
                        <img className='panel-img' src="images/authorize.png"/>
                        <span className='panel-span'>ZALOGUJ SIĘ</span>
                    </a>
                </li>

                <li className='nav-element panel szukaj'>
                    <a className='link' href="#">
                        <img className='panel-img' src="images/search.png"/>
                        <span className='panel-span find'>SZUKAJ</span>
                    </a>
                </li>
            </ul>
        </nav>;
    }
}
import React from "react";
import './Footer.scss';

export interface FooterProps {
    className?: string;
}

export class Footer extends React.Component<FooterProps, {}> {
    render(): JSX.Element {
        return <footer className={`${this.props.className} Footer`}>
            <ul>
                <li className="socialimg firstel"><a href=""><img className="social" src="images/fb.png" alt="fb"/></a>
                </li>
                <li className="socialimg"><a href=""><img className="social" src="images/ig.png" alt="ig"/></a></li>
                <li className="socialimg"><a href=""><img className="social" src="images/twitter.png"
                                                          alt="twitter"/></a></li>
                <li className="socialtxt">Copyright &copy; by Alicja Kozik & Wiktoria Jancewicz</li>
            </ul>
        </footer>
    }
}
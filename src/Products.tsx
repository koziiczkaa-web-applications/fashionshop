import React from 'react';
import './Products.scss';

export interface ProductsProps {
    className?: string;
}

export class Products extends React.Component<ProductsProps, {}> {
    render(): JSX.Element {
        return <div className={`${this.props.className} Products`}>
            <ul className="companies">
                <li className="element">
                    <a href="produkty2.php?marka=1">
                        <img className="company" src="images/thlogo.png" alt=""/>
                    </a>
                </li>
                <li className="element">
                    <a href="produkty2.php?marka=2">
                        <img className="company" src="images/cklogo.png" alt=""/>
                    </a>
                </li>
                <li className="element">
                    <a href="produkty2.php?marka=3">
                        <img className="company" src="images/rllogo.png" alt=""/>
                    </a>
                </li>
                <li className="element">
                    <a href="produkty2.php?marka=5">
                        <img className="company" src="images/lacostelogo.png" alt=""/>
                    </a>
                </li>
                <li className="element">
                    <a href="produkty2.php?marka=4">
                        <img className="company" src="images/vanslogo.png" alt=""/>
                    </a>
                </li>
            </ul>
        </div>;
    }
}
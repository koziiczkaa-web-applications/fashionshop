import React from 'react';
import './App.scss';
import {NavBar} from "./NavBar";
import {Products} from "./Products";
import {Footer} from "./Footer";
import {CustomCarousel} from "./CustomCarousel";

function App() {
    return (
        <div className='App'>
            <NavBar/>
            <CustomCarousel/>
            <div className="message">
                <h5 className="companiesmsg">Wybierz firmę, której produkty chcesz wyświetlić</h5>
            </div>
            <Products/>
            <Footer/>
        </div>
    );
}

export default App;
